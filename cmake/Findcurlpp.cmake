include(FindPackageHandleStandardArgs)

find_path(curlpp_INCLUDE_DIR
        NAMES curlpp/cURLpp.hpp
        )

find_library(curlpp_LIBRARY
        NAMES curlpp
        )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(curlpp  DEFAULT_MSG
                                  curlpp_LIBRARY curlpp_INCLUDE_DIR)

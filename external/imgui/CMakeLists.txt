cmake_minimum_required(VERSION 3.10)
project(imgui)

set(OpenGL_GL_PREFERENCE "LEGACY")

find_package(SDL2 REQUIRED)
find_package(OpenGL REQUIRED)

set(SRC_GRP_IMGUI
    imgui/imgui.cpp
    imgui/imgui_draw.cpp
    imgui/imgui_widgets.cpp
    imgui/imgui_demo.cpp
    imgui/misc/cpp/imgui_stdlib.cpp
)

set(SRC_GRP_IMGUI_SDL2_IMPL
    imgui/examples/imgui_impl_sdl.cpp
    imgui/examples/imgui_impl_opengl3.cpp
)

set(SRC_GRP_IMGUI_GL3W
    imgui/examples/libs/gl3w/GL/gl3w.c
)

add_library(imgui STATIC
    ${SRC_GRP_IMGUI}
    ${SRC_GRP_IMGUI_SDL2_IMPL}
    ${SRC_GRP_IMGUI_GL3W}
)

target_compile_definitions(imgui PUBLIC IMGUI_IMPL_OPENGL_LOADER_GL3W)

target_include_directories(imgui INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}
)

target_include_directories(imgui PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/imgui/examples/libs/gl3w
  ${SDL2_INCLUDE_DIRS}
)



target_include_directories( imgui SYSTEM PRIVATE
  ${CMAKE_CURRENT_SOURCE_DIR}/imgui
  ${CMAKE_CURRENT_SOURCE_DIR}/imgui/examples
)

target_link_libraries(
    imgui
    SDL2
    OpenGL::GLX
    ${CMAKE_DL_LIBS}
)


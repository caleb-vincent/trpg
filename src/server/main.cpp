// Copyright (c) 2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "common/codec.hpp"
#include "common/net.hpp"

#include <server_https.hpp>

#include <future>
#include <memory>

using namespace std::string_literals;
using HttpServer = SimpleWeb::Server<SimpleWeb::HTTPS>;

int main(int argc, char* argv[])
{
   auto const crt{ argc < 3 ? "localhost.crt"s : argv[1] };
   auto const key{ argc < 3 ? "localhost.key"s : argv[2] };

   std::cout << argv[0] << " <" << crt << " " << key << ">\n";

   HttpServer server(crt, key);
   server.config.port = trpg::net::SERVER_PORT;
   
   server.resource["\\d+\\/(.*)"]["POST"] = [](std::shared_ptr<HttpServer::Response> response, std::shared_ptr<HttpServer::Request> request)
   {
      auto const msg{ trpg::Codec::SingleDelivery(request->content.string()) };
      std::cout << request->path_match[1].str() <<" : " << msg.Dump() << std::endl;
      response->write("OK");
   };

   server.on_error = [](std::shared_ptr<HttpServer::Request>, const SimpleWeb::error_code & ec)
   {
      std::cerr << "ERROR: " << ec << '\n';
   };
   
   std::promise<unsigned short> server_port;
   std::thread server_thread([&server, &server_port]()
   {
      // Start server
      server.start([&server_port](unsigned short port) 
      {
         server_port.set_value(port);
      });
   });
   std::cout << "Server listening on port " << server_port.get_future().get() << std::endl << std::endl;
   
   server_thread.join();
}

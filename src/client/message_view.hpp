//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "common/message.hpp"
#include "common/message_repo.hpp"

#include <imgui/imgui.h>

#include <string>
#include <vector>


namespace trpg::client
{
   struct Context;

   class MessageView
   {
   public:
      static int constexpr PAGE_MESSAGE_COUNT{ 64 };

      MessageView(Context& context);

      void Frame();

   private:
      static const ImVec4 CHARACTER_NAME_COLOR;
      static const ImVec4 TIMESTAMP__COLOR;

      void MessagesUpdate();

      Context& m_context;

      int m_page{ 1 };
      MessageRepo::Time_t::time_point m_lastUpdate;
      std::vector<Message> m_msgs;

   };
}

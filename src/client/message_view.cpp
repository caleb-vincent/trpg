//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "message_view.hpp"

#include "context.hpp"
#include "message_manager.hpp"

#include <imgui/imgui.h>

#include <algorithm>
#include <ctime>
#include <cmath>
#include <iostream>
#include <sstream>

using namespace trpg::client;

const ImVec4 MessageView::CHARACTER_NAME_COLOR{ 0.5f, 0.85f, 0.5f, 1.0f };
const ImVec4 MessageView::TIMESTAMP__COLOR{ 0.4f, 0.4f, 0.4f, 1.0f };

using namespace std::chrono_literals;

MessageView::MessageView::MessageView(Context& context) :
   m_context{ context },
   m_lastUpdate{ 0s }
{}

void MessageView::Frame()
{
   MessagesUpdate();
   
   ImGui::BeginChild("Mesage Log");
   
   auto const pageCount{ static_cast<int>(std::ceil( m_msgs.size() / (float)PAGE_MESSAGE_COUNT)) };
   
   
   if(pageCount > 1)
   {
      if (ImGui::ArrowButton("##left", ImGuiDir_Left))
      {}

      ImGui::SameLine();

      std::stringstream scrollText;
      scrollText << "Page %d of " << pageCount;
      ImGui::SliderInt("", &m_page, 1, pageCount, scrollText.str().c_str());

      ImGui::SameLine();

      if (ImGui::ArrowButton("##right", ImGuiDir_Right))
      {}
   }
   
   {      
      ImGui::BeginChild("Message Scrolling");
   
      int const start{ static_cast<int>(m_msgs.size() - (m_page - 1) * PAGE_MESSAGE_COUNT - 1) };
      auto const end{ [&]
         {
            const auto temp{ static_cast<int>(m_msgs.size()) - m_page * (int)PAGE_MESSAGE_COUNT  };
            if(temp > 0)
            {
               return temp;
            }
            else
            {
               return 0;
            }   
         }() };
      for( auto indx{start}; indx >= end; --indx)
      {
         const auto msg{ m_msgs[indx] };
         
         ImGui::TextColored(CHARACTER_NAME_COLOR, "%s", msg.Sender().c_str());
         ImGui::SameLine();
         std::time_t const timeStamp{ Message::TimeStampClock_t::to_time_t(msg.TimeStamp()) };
         ImGui::TextColored(TIMESTAMP__COLOR, "%s", std::ctime(&timeStamp));
         switch(msg.GetState())
         {
            case Message::State::SENT:
               break;
            case Message::State::SENDING:
               ImGui::SameLine();
               ImGui::TextColored({1.0f, 1.0f, 0.0f, 1.0f}, "SENDING");
               break;
            case Message::State::QUEUED:
               ImGui::SameLine();
               ImGui::TextColored({1.0f, 1.0f, 0.0f, 1.0f}, "QUEUED");
               break;
            default:
               ImGui::SameLine();
               ImGui::TextColored({1.0f, 0.0f, 0.0f, 1.0f}, "ERROR");
               break;
         }
         
         ImGui::TextWrapped("%s", msg.Text().c_str());
         ImGui::Separator();
      }
      
      ImGui::EndChild();
   }
   
   ImGui::EndChild();
}   

void MessageView::MessagesUpdate()
{
   if(m_context.db.HasChangedSince(m_lastUpdate))
   {
      m_msgs = m_context.db.GetAll();
      m_lastUpdate = MessageRepo::Time_t::now();
   }
}

//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "config.hpp"
#include "config_view.hpp"
#include "context.hpp"
#include "main_menu.hpp"
#include "message_view.hpp"

#include "imgui/default_application.hpp"

#include "message_manager.hpp"

#include "common/db.hpp"

#include <imgui/imgui.h>

#include <algorithm>


static constexpr auto APPLICATION_TITLE{ "TRPG" };
static constexpr auto APPLICATION_WIDTH{ 1280 };
static constexpr auto APPLICATION_HEIGHT{ 720 };

namespace trpg::client
{
   class Application : public DefaultApplication
   {
   public:
      explicit Application () :
      DefaultApplication(APPLICATION_TITLE, APPLICATION_WIDTH, APPLICATION_HEIGHT),
         m_messageManager{ m_config, m_db.GetMessageRepo() },
         m_msgBuffer(1024u, 0),
         m_context{ m_db.GetMessageRepo(), m_config },
         m_mainMenu{ m_context },
         m_messageView{ m_context }
      {}

      void render() override
      {

         m_mainMenu.Frame();

         if(m_mainMenu.showConfig)
         {
            ConfigView(m_config, m_mainMenu.showConfig);
         }

         if(m_mainMenu.ShowDemo())
         {
            ImGui::ShowDemoWindow();
         }

         {
            auto const windowSize{ getWindowSize() };
            ImGui::SetNextWindowSize({static_cast<float>(windowSize.w), static_cast<float>(windowSize.h-20)}, ImGuiCond_FirstUseEver);
            ImGui::SetNextWindowPos({0,20});
            ImGui::Begin("Group", &m_showAnother, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

            ImGui::InputTextMultiline("##Message", &m_msgBuffer[0], m_msgBuffer.capacity());
            ImGui::SameLine();
            if(ImGui::Button("Send"))
            {
               m_messageManager.Send(m_msgBuffer);
            }

            m_messageView.Frame();

            ImGui::End();
         }
      }

      void update() override
      {
         m_messageManager.Update();
      }

      void postUpdate() override
      {}

   private:

      Db m_db;
      Config m_config;
      MessageManager m_messageManager;
      std::string m_msgBuffer;

      Context  m_context;

      MainMenu m_mainMenu;
      MessageView m_messageView;

      bool m_showAnother{ true };
   };
}

int main(int, char**)
{
   trpg::client::Application app;
   
   app.run();
   
   return 0;
}

//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "message_manager.hpp"

#include "common/codec.hpp"
#include "common/db.hpp"
#include "common/net.hpp"

#include <chrono>
#include <memory>
#include <string>

#include <client_https.hpp>

using HttpClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

using namespace std::string_literals;
using namespace trpg::client;

static const auto TEST_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."s;

MessageManager::MessageManager(Config const& config, MessageRepo& db) :
   m_config{ config },
   m_msgDb{ db }
{
   if(m_msgDb.GetAll().empty())
   {
      using namespace std::chrono_literals;
      Message::TimeStamp_t time{ 400000h };
      for(auto i{0}; i < 10; ++i)
      {
         m_msgDb.Insert({ m_config.userId, m_config.groupId, TEST_TEXT, time, Message::State::QUEUED });
         time += 7s;
      }
   }
}

void MessageManager::Send( std::string msg )
{
   m_msgDb.Insert({m_config.userId, m_config.groupId, msg, std::chrono::system_clock::now(), Message::State::QUEUED});
}

void MessageManager::Update()
{
   using namespace std::chrono_literals;
   
   auto const startTime{ std::chrono::steady_clock::now() };
   
   if(m_msgDb.HasChangedSince(m_lastUpdate))
   {
      m_lastUpdate = MessageRepo::Time_t::now();

      if(auto queuedMsgs{ m_msgDb.GetByStatus(Message::State::QUEUED) };
         !queuedMsgs.empty())
      {
         HttpClient client([this]()
         {
            char buf[16] = {0};
            snprintf(buf, 15, "%u", net::SERVER_PORT);
            return m_config.server + ":" + buf;
         }(), m_config.server != "localhost");
         for(auto msg : queuedMsgs)
         {
            msg.SetState(Message::State::SENDING);

            auto const msgKey{ msg.GetKey() };
            client.request("POST", "0/" + msg.Sender(), Codec::SingleDelivery(msg), [this, msgKey](std::shared_ptr<HttpClient::Response>, const SimpleWeb::error_code &ec)
               {
                  auto msg{ m_msgDb.GetByKey(msgKey) };
                  if(!ec)
                  {
                     msg.SetState(Message::State::SENT);
                  }
                  else
                  {
                     std::cerr << "Client request error: " << ec.message() << '\n';
                     msg.SetState(Message::State::QUEUED);
                  }
                  m_msgDb.Update(msg);
               });

            auto const now{ std::chrono::steady_clock::now() };
            if(auto const diff{ now - startTime };
               diff > 10ms)
            {
               break;
            }
         }

         m_msgDb.Update(queuedMsgs);

         client.io_service->run();
      }
      else if(auto sentMessages{ m_msgDb.GetByStatus(Message::State::SENDING) };
              !sentMessages.empty())
      {
        for( auto msg : sentMessages)
        {
           msg.SetState(Message::State::SENT);
        }
        m_msgDb.Update(sentMessages);
      }

      //! @todo need to check periodically for any messages that got stuck in SENDING
   }
}

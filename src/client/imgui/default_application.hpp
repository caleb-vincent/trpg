/*
 * 
 * Copyright (c) 2019 Caleb Vincent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <stdint.h>
#include <chrono>

struct SDL_Window;
typedef void *SDL_GLContext;

class DefaultApplication
{
public:   
   DefaultApplication(char const*const title, uint32_t const w, uint32_t const h);
   virtual ~DefaultApplication();
   void run();
   
   
protected:
   
   enum class EventResult
   {
      CONTINUE,
      QUIT
   };
   
   struct WindowSize
   {
      int w{0}, h{0};
   };
   
   virtual EventResult doEvents();
   
   virtual void update() = 0;
   virtual void postUpdate() = 0;
   virtual void render();
   
   WindowSize getWindowSize() const;
   
private:
   SDL_Window* window;
   SDL_GLContext context;
   
   uint64_t frameCount{ 0 };
   std::chrono::milliseconds updatePeriod;
   
   std::chrono::time_point<std::chrono::steady_clock> nextUpdate;
};

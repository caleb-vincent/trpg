/*
 * 
 * Copyright (c) 2019 Caleb Vincent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "default_application.hpp"

#include <GL/gl3w.h>
#include <imgui/imgui.h>
#include <SDL.h>

#include "opengl3.h"
#include "sdl.h"

#if __APPLE__
// GL 3.2 Core + GLSL 150
static constexpr auto GLSL_VERSION{ "#version 150" }; 
#else
// GL 3.0 + GLSL 130
static constexpr auto GLSL_VERSION{ "#version 130" }; 
#endif

static const ImVec4 CLEAR_COLOR{ ImVec4(0.45f, 0.55f, 0.60f, 1.00f) };

using namespace std::chrono_literals;

DefaultApplication::DefaultApplication(char const*const title, uint32_t const w, uint32_t const h) :
   updatePeriod{ 50ms },
   nextUpdate{ std::chrono::steady_clock::now() + updatePeriod }
{
   // Setup SDL
   if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
   {
      printf("Error: %s\n", SDL_GetError());
      exit(-1);   //! @todo Be better
   }   
   
   // Decide GL+GLSL versions
   #if __APPLE__
   // GL 3.2 Core + GLSL 150
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
   #else
   // GL 3.0 + GLSL 130
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
   #endif
   
   // Create window with graphics context
   SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
   SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
   SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
   
   auto const windowFlags = static_cast<SDL_WindowFlags>(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
   window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, windowFlags);
   context = SDL_GL_CreateContext(window);
   
   SDL_GL_MakeCurrent(window, context);
   SDL_GL_SetSwapInterval(1); // Enable vsync  
   
   // Initialize OpenGL loader
   #if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
   bool err = gl3wInit() != 0;
   #elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
   bool err = glewInit() != GLEW_OK;
   #elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
   bool err = gladLoadGL() == 0;
   #else
   bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
   #endif
   if (err)
   {
      fprintf(stderr, "Failed to initialize OpenGL loader!\n");
      exit(1); // @todo Do this better
   }
   
   
   // Setup Dear ImGui context
   IMGUI_CHECKVERSION();
   ImGui::CreateContext();
   
   ImGui::StyleColorsDark();
   
   // Setup Platform/Renderer bindings
   ImGui_ImplSDL2_InitForOpenGL(window, context);
   ImGui_ImplOpenGL3_Init(GLSL_VERSION);
}

DefaultApplication::~DefaultApplication()
{
   ImGui_ImplOpenGL3_Shutdown();
   ImGui_ImplSDL2_Shutdown();
   ImGui::DestroyContext();
   
   SDL_GL_DeleteContext(context);
   SDL_DestroyWindow(window);
   SDL_Quit();
}


void DefaultApplication::run()
{
   while (doEvents() != EventResult::QUIT)
   {
      if(std::chrono::steady_clock::now() > nextUpdate)
      {
         update();
         nextUpdate = std::chrono::steady_clock::now() + updatePeriod;
         postUpdate();
      }
      
      // Start the Dear ImGui frame
      ImGui_ImplOpenGL3_NewFrame();
      ImGui_ImplSDL2_NewFrame(window);
      ImGui::NewFrame();
      
      render();
      ++frameCount;
      
      auto io{ ImGui::GetIO() };
      
      // Rendering
      ImGui::Render();
      glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
      glClearColor(CLEAR_COLOR.x, CLEAR_COLOR.y, CLEAR_COLOR.z, CLEAR_COLOR.w);
      glClear(GL_COLOR_BUFFER_BIT);
      ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
      SDL_GL_SwapWindow(window);
   }
}

DefaultApplication::EventResult DefaultApplication::doEvents()
{
   // Poll and handle events (inputs, window resize, etc.)
   // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
   // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
   // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
   // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
   SDL_Event event;
   while (SDL_PollEvent(&event))
   {
      ImGui_ImplSDL2_ProcessEvent(&event);
      if (event.type == SDL_QUIT)
      {
         return EventResult::QUIT;
      }
      
      if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
      {
         return EventResult::QUIT;
      }
   }
   return EventResult::CONTINUE;
}

void DefaultApplication::render()
{
   ImGui::ShowDemoWindow();
}

DefaultApplication::WindowSize DefaultApplication::getWindowSize() const
{
   WindowSize size;
   SDL_GetWindowSize(window, &size.w, &size.h);
   return size;
}

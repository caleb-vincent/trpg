//
// Copyright (c) 2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "config_view.hpp"

#include "config.hpp"

#include <imgui/imgui.h>
#include <imgui/misc/cpp/imgui_stdlib.h>

void trpg::client::ConfigView(Config& config, bool& stayOpen)
{
   ImGui::OpenPopup("Config");
   
   if (ImGui::BeginPopupModal("Config", NULL, ImGuiWindowFlags_AlwaysAutoResize))
   {
      ImGui::InputText("User Id", &config.userId);
      
      ImGui::InputText("Group Id", &config.groupId);
      
      ImGui::InputText("Server", &config.server);
      
      if (ImGui::Button("Close"))
      {
         ImGui::CloseCurrentPopup();
         stayOpen = false;
      }
      ImGui::EndPopup();
   }
}   


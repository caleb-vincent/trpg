//
// Copyright (c) 2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "message_repo.hpp"

#include "storage.hpp"

using namespace trpg;
using namespace sqlite_orm;

MessageRepo::MessageRepo(Storage& store) :
   m_storage{ store },
   m_lastUpdate{ Time_t::now() }
{}

std::vector<Message> MessageRepo::GetAll() const
{
   return m_storage->get_all<Message>();
}

Message MessageRepo::GetByKey(const Message::Key_t key) const
{
   return m_storage.get<Message>(key);
}

std::vector<Message> MessageRepo::GetByUserId(const std::string& userId) const
{
   return m_storage.get_all<Message>(where(c(&Message::m_sender) == userId));
}

std::vector<Message> MessageRepo::GetByGroupId(const std::string& groupId) const
{
   return m_storage.get_all<Message>(where(c(&Message::m_group) == groupId));
}

std::vector<Message> MessageRepo::GetByStatus(const Message::State status) const
{
   return m_storage.get_all<Message>(where(c(&Message::m_state) == status));
}

Message::Key_t MessageRepo::Insert(Message msg)
{
   m_lastUpdate = Time_t::now();
   return m_storage.insert<Message>(msg);
}

void MessageRepo::Update(Message const& msg)
{
   m_lastUpdate = Time_t::now();
   m_storage.update<Message>(msg);
}

void MessageRepo::Update(std::vector<Message> const& msgs)
{
   m_lastUpdate = Time_t::now();
   m_storage.transaction([&]
      {
         for( auto const msg : msgs)
         {
            m_storage.update(msg);
         }
         return true;
      });
}

bool MessageRepo::HasChangedSince(const Time_t::time_point& time) const
{
   return  m_lastUpdate > time;
}





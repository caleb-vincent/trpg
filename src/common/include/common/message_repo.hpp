﻿#pragma once
//
// Copyright (c) 2020  Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "message.hpp"

#include <memory>
#include <vector>

namespace trpg
{
   class Storage;

   class MessageRepo
   {
   public:
      using Time_t =  std::chrono::steady_clock;

      MessageRepo(Storage& storage);
      ~MessageRepo() = default;

      std::vector<Message> GetAll() const;
      Message GetByKey(Message::Key_t const key) const;
      std::vector<Message> GetByUserId(std::string const& userId) const;
      std::vector<Message> GetByGroupId(std::string const& groupId) const;
      std::vector<Message> GetByStatus(Message::State const status) const;

      Message::Key_t Insert(Message msg);
      void Update(Message const& msg);
      void Update(std::vector<Message> const& msgs);

      bool HasChangedSince(Time_t::time_point const& time) const;

   private:
      Storage& m_storage;

      Time_t::time_point m_lastUpdate;
   };
}

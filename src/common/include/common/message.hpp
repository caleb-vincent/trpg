//
// Copyright (c) 2018,2020  Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include <chrono>
#include <string>

namespace trpg
{   
   class Message
   {
   public:
      friend auto CreateStorage();
      friend class MessageRepo;
      
      //! @todo The time stamp should really be UTC time in milliseconds. It shouldn't be local, and it should be steady
      using TimeStampClock_t = std::chrono::system_clock;
      using TimeStamp_t = TimeStampClock_t::time_point;
      
      using Key_t = size_t;
      static constexpr auto INVALID_KEY{ std::numeric_limits<Key_t>::max() };

      enum class State
      {
         NONE,
         QUEUED,
         SENDING,
         SENT
      };
      
      Message() = default;
      ~Message() = default;
      
      std::string const& Text() const;
      
      TimeStamp_t const& TimeStamp() const;
      
      std::string const& Sender() const;
      std::string const& Group() const;
      
      State GetState() const;

      Key_t GetKey() const;
      
      Message(std::string sender, std::string group, std::string message, TimeStamp_t timeStamp, State state = State::NONE);
      Message(std::string sender, std::string group, std::string message, State state = State::NONE);


      void SetState(State state);

      std::string Dump() const;

   private:
      Key_t       m_key { INVALID_KEY };
      std::string m_sender;
      std::string m_group;
      std::string m_message;
      TimeStamp_t m_timeStamp{ std::chrono::seconds(0) };
      State       m_state { State::NONE };
   };
}

cmake_minimum_required(VERSION 3.8)

file(GLOB trpg_common_SRC "*.?pp")
file(GLOB trpg_common_INC "include/common/*.hpp")

add_subdirectory(codec)

add_library(trpg_common STATIC
   ${trpg_common_SRC}
   ${trpg_common_INC})

   
target_include_directories(trpg_common PRIVATE include/common) #allows space_core to treat includes as if they are in the local directory

target_include_directories(trpg_common PUBLIC include)    #but other libaries need to use the namespace/folder

find_library(sqlite_orm REQUIRED)

target_link_libraries(trpg_common
  sqlite_orm
  sqlite3
  simple-web-server
  date
  TRpg::Codec)
  
set_property(TARGET trpg_common PROPERTY CXX_STANDARD 20)

target_compile_options(trpg_common PRIVATE "-Wall")
target_compile_options(trpg_common PRIVATE "-Wpedantic")
target_compile_options(trpg_common PRIVATE "-Wextra")
target_compile_options(trpg_common PRIVATE "-Werror")

add_library(TRpg::Common ALIAS trpg_common)


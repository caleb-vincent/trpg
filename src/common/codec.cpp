//
// Copyright (c) 2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "codec.hpp"

#include "message.hpp"

#include "codec/CodecMsg.h"
#include "codec/CodecMsgElement.h"

#include <date/date.h>

#include <chrono>
#include <cstdio>
#include <sstream>

using namespace trpg;

namespace
{

   //! @todo Consider wrapping the whole ASN.1 object to handle alloc and free-ing
   template<typename T>
   T* Calloc()
   {
      return (T*)calloc(1, sizeof(T));
   }

   std::string OctetString2String(OCTET_STRING_t const& octetString)
   {
      return{ reinterpret_cast<char const*>(octetString.buf), static_cast<size_t>(octetString.size) };
   }

   int TimeStamp2Gt(Message::TimeStamp_t const& time, GeneralizedTime_t& timeStr)
   {
      using namespace std::chrono;
      using namespace date;
      return OCTET_STRING_fromString(&timeStr, format("%Y%m%d%H%M%S%z", floor<milliseconds>(time)).c_str());
   }

   int Gt2TimeStamp(GeneralizedTime_t const& timeStr, Message::TimeStamp_t& time)
   {
      using namespace std::chrono;
      using namespace date;
      std::istringstream in{ OctetString2String(timeStr) };
      in >> parse("%Y%m%d%H%M%S%z", time);
      return in.fail() ? -1 : 0;
   }

   int WriteOutCallback(const void *buffer, size_t size, void *key)
   {
      Codec::EncodedMsg_t& out{ *reinterpret_cast<Codec::EncodedMsg_t*>(key) };
      auto const pBuffer{ reinterpret_cast<Codec::EncodedMsg_t::value_type const*>(buffer) };
      out.insert(out.end(), &pBuffer[0], &pBuffer[size]);
      return static_cast<int>(out.size());
   }
}

Codec::EncodedMsg_t Codec::SingleDelivery(Message const& msg)
{
   auto*const pCommMsg{ Calloc<CodecDelivery_t>() };

   OCTET_STRING_fromString(&pCommMsg->userId, msg.Sender().c_str());
   OCTET_STRING_fromString(&pCommMsg->message, msg.Text().c_str());
   OCTET_STRING_fromString(&pCommMsg->groupId, msg.Group().c_str());
   TimeStamp2Gt(msg.TimeStamp(), pCommMsg->timestamp);

   Codec::EncodedMsg_t outBuffer;
   if(asn_check_constraints(&asn_DEF_CodecDelivery, (void*)pCommMsg, nullptr, nullptr) == 0)
   {
      uper_encode(&asn_DEF_CodecDelivery, pCommMsg, WriteOutCallback, &outBuffer);
   }

   ASN_STRUCT_FREE(asn_DEF_CodecDelivery, pCommMsg);

   return outBuffer;
}

Message Codec::SingleDelivery(EncodedMsg_t const& msg)
{
   CodecDelivery_t * pCommMsg{ nullptr };
   if(uper_decode(0, &asn_DEF_CodecDelivery, (void**)&pCommMsg, msg.data(), msg.size(), 0, 0).code == RC_OK &&
      pCommMsg != nullptr &&
      asn_check_constraints(&asn_DEF_CodecDelivery, (void*)pCommMsg, nullptr, nullptr) == 0)
   {
      std::string userId{ OctetString2String(pCommMsg->userId) };
      std::string groupId{ OctetString2String(pCommMsg->groupId) };
      std::string message{ OctetString2String(pCommMsg->message) };

      Message::TimeStamp_t timeStamp;
      Gt2TimeStamp(pCommMsg->timestamp, timeStamp);

      ASN_STRUCT_FREE(asn_DEF_CodecDelivery, pCommMsg);

      return { userId, groupId, message, timeStamp };
   }

   ASN_STRUCT_FREE(asn_DEF_CodecDelivery, pCommMsg);

   return {};
}

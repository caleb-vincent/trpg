//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "common/message.hpp"

#include "message_db_adapter.hpp"

#include <sqlite_orm/sqlite_orm.h>

#include <chrono>

namespace trpg
{
   inline auto CreateStorage()
   {
      return sqlite_orm::make_storage("db.sqlite",
                                      sqlite_orm::make_table<Message>( "messages",
                                                               sqlite_orm::make_column("key", &trpg::Message::m_key, sqlite_orm::primary_key(), sqlite_orm::autoincrement()),
                                                               sqlite_orm::make_column("timestamp", &trpg::Message::m_timeStamp),
                                                               sqlite_orm::make_column("user-id", &trpg::Message::m_sender),
                                                               sqlite_orm::make_column("group", &trpg::Message::m_group),
                                                               sqlite_orm::make_column("message", &trpg::Message::m_message),
                                                               sqlite_orm::make_column("state", &trpg::Message::m_state)));
   }
   
   class Storage
   {
   public:
      using Storage_t = decltype(CreateStorage());
      using Time_t =  std::chrono::steady_clock;
      
      Storage() :
         m_storage{ CreateStorage() }
      {
         m_storage.sync_schema();
      }
      
      ~Storage()
      {
         m_storage.sync_schema();
      };

      template<class O, class... Args>
      auto get(Args... args) const
      {
         return m_storage.get<O>(std::forward<Args>(args)...);
      }

      template<class O, class... Args>
      auto get_all(Args... args) const
      {
         return m_storage.get_all<O>(std::forward<Args>(args)...);
      }

      template<class... Args>
      auto insert(Args... args)
      {
         m_lastUpdate = Time_t::now();
         return m_storage.insert(std::forward<Args>(args)...);
      }

      template<class O, class... Args>
      auto replace(Args... args)
      {
         m_lastUpdate = Time_t::now();
         return m_storage.replace<O>(std::forward<Args>(args)...);
      }

      template<class... Args>
      auto select(Args... args) const
      {
         return m_storage.select(std::forward<Args>(args)...);
      }

      template<class... Args>
      auto sync_schema(Args... args)
      {
         m_lastUpdate = Time_t::now();
         return m_storage.sync_schema(std::forward<Args>(args)...);
      }

      template<class... Args>
      auto transaction(Args... args) const
      {
         return m_storage.transaction(std::forward<Args>(args)...);
      }

      template<class... Args>
      auto transaction(Args... args)
      {
         m_lastUpdate = Time_t::now();
         return m_storage.transaction(std::forward<Args>(args)...);
      }

      template<class... Args>
      auto update(Args... args)
      {
         m_lastUpdate = Time_t::now();
         return m_storage.update(std::forward<Args>(args)...);
      }

      bool HasChangedSince(Time_t::time_point const& time) const
      {
         return  m_lastUpdate > time;
      }

      Storage_t* operator->()
      {
         m_lastUpdate = Time_t::now();
         return &m_storage;
      }


   private:
      mutable Storage_t m_storage;

      Time_t::time_point m_lastUpdate{ Time_t::now() };
   };
   
}

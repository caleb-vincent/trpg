//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "message.hpp"

#include <date/date.h>

#include <sqlite_orm/sqlite_orm.h>

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <type_traits>

//! @todo There has got to be some better way to add custom type support to sqlite_orm

namespace sqlite_orm
{
   template<>
   struct type_printer<trpg::Message::TimeStamp_t> : public integer_printer {};
   
   template<>
   struct row_extractor<trpg::Message::TimeStamp_t, void>
   {
      static_assert (std::is_same<decltype(strtoll("", std::declval<char**>(), 0)),
                                  decltype(sqlite3_column_int64(nullptr, 0))>::value);

      trpg::Message::TimeStamp_t extract(const char *row_value)
      {

         return trpg::Message::TimeStamp_t(std::chrono::milliseconds(strtoll(row_value, nullptr, 10)));
      }
      
      trpg::Message::TimeStamp_t extract(sqlite3_stmt *stmt, int columnIndex)
      {
         auto const ret{ sqlite3_column_int64(stmt, columnIndex) };
         return trpg::Message::TimeStamp_t(std::chrono::milliseconds(ret));
      }
   };
   
   template<>
   struct statement_binder<trpg::Message::TimeStamp_t>
   {

      int bind(sqlite3_stmt *stmt, int index, const trpg::Message::TimeStamp_t &value)
      {
         static_assert (std::is_same<int64_t, decltype(value.time_since_epoch().count())>::value);
         using namespace std::chrono;

         auto const ret{ std::chrono::duration_cast<milliseconds>(value.time_since_epoch()).count() };
         return sqlite3_bind_int64(stmt, index, ret);
      }
   };

   template<>
   struct type_printer<trpg::Message::State> : public integer_printer {};
   
   template<>
   struct row_extractor<trpg::Message::State, void>
   {
      trpg::Message::State extract(const char *row_value)
      {
         return static_cast<trpg::Message::State>(atoi(row_value));
      }
      
      trpg::Message::State extract(sqlite3_stmt *stmt, int columnIndex)
      {
         return static_cast<trpg::Message::State>(sqlite3_column_int(stmt, columnIndex));
      }
   };
   
   template<>
   struct statement_binder<trpg::Message::State>
   {
      int bind(sqlite3_stmt *stmt, int index, const trpg::Message::State &value)
      {
         return sqlite3_bind_int(stmt, index, static_cast<int>(value));
      }
   };
}


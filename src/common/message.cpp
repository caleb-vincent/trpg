//
// Copyright (c) 2018,2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "message.hpp"

#include <date/date.h>

#include <sstream>


using namespace trpg;


std::string const& Message::Text() const
{
   return m_message;
}

Message::TimeStamp_t const& Message::TimeStamp() const
{
   return m_timeStamp;
}

std::string const& Message::Sender() const
{
   return m_sender;
}

std::string const& Message::Group() const
{
   return m_group;
}

Message::State Message::GetState() const
{
   return m_state;
}


Message::Key_t Message::GetKey() const
{
   return m_key;
}

void Message::SetState(State state)
{
   m_state = state;
}

Message::Message(std::string sender, std::string group, std::string message, TimeStamp_t timeStamp, State state) :
   m_key{ INVALID_KEY },
   m_sender{ sender },
   m_group{ group },
   m_message{ message },
   m_timeStamp{ timeStamp },
   m_state{ state }
{}

Message::Message(std::string sender, std::string group, std::string message, State state) :
   m_key{ INVALID_KEY },
   m_sender{ sender },
   m_group{ group },
   m_message{ message },
   m_timeStamp{ TimeStampClock_t::now() },
   m_state{ state }
{}

std::string Message::Dump() const
{
   using namespace date;

   std::stringstream stream;
   stream << m_key << " " << m_sender << " " << m_group << " " << m_timeStamp << " " << (int)m_state;
   return stream.str();
}

// Copyright (c) 2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <catch2/catch.hpp>

#include "common/db.hpp"

using namespace std::chrono_literals;

SCENARIO("Message Repo Insert/Get")
{
   constexpr auto sender{ "Test-Sender" };
   constexpr auto group { "Test-Group" };
   constexpr auto text{ "Test-Message"};
   const auto time{ trpg::Message::TimeStampClock_t::now() };
   constexpr auto state{ trpg::Message::State::QUEUED };

   trpg::Db db;

   auto const msgKey{ db.GetMessageRepo().Insert({ sender, group, text, time, state }) };

   REQUIRE(msgKey != trpg::Message::INVALID_KEY);

   auto const msg{ db.GetMessageRepo().GetByKey(msgKey) };

   REQUIRE(msg.GetKey() == msgKey);
   REQUIRE(msg.Sender() == sender);
   REQUIRE(msg.Group() == group);
   REQUIRE(msg.Text() == text);
   REQUIRE(msg.TimeStamp() == std::chrono::floor<std::chrono::milliseconds>(time));
   REQUIRE(msg.GetState() == state);
}

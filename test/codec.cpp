// Copyright (c) 2020 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <catch2/catch.hpp>

#include "common/codec.hpp"
#include "common/debug.hpp"
#include "common/message.hpp"

#include <chrono>
#include <iostream>
#include <ostream>
#include <string>

using namespace std::chrono_literals;

SCENARIO("Encode/Decode a SingleDelivery")
{
   auto const sender{ "Test-Sender" };
   auto const group{ "Test-Group" };
   auto const text{ "Test-Message" };
   auto const time{ trpg::Message::TimeStampClock_t::now() };
   trpg::Message msg(sender, group, text, time);

   auto const encodedMsg{ trpg::Codec::SingleDelivery(msg) };
   trpg::HexDump(&encodedMsg[0], encodedMsg.size(), std::cout);

   trpg::Message msg2{ trpg::Codec::SingleDelivery(encodedMsg) };
   std::cout << '\n' << msg2.Dump() << '\n';

   REQUIRE(msg2.Text() == text);
   REQUIRE(msg2.Sender() == sender);
   REQUIRE(msg2.Group() == group);
   REQUIRE(msg2.TimeStamp() == std::chrono::floor<std::chrono::milliseconds>(time));
}

SCENARIO("Failure to Encode invalid UTF8 message")
{
   auto const sender{ "Test-Sender" };
   auto const group{ "Test-Group" };
   auto const time{ trpg::Message::TimeStampClock_t::now() };
   std::string text{ "Test-Message" };
   text[4] = (char)0xC0;
   text += (char)0xC2;

   trpg::Message msg(sender, group, text, time);

   auto const encodedMsg{ trpg::Codec::SingleDelivery(msg) };
   trpg::Message msg2{ trpg::Codec::SingleDelivery(encodedMsg) };

   REQUIRE(msg2.Text() != text);
}
